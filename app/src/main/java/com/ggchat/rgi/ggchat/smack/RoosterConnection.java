package com.ggchat.rgi.ggchat.smack;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ggchat.rgi.ggchat.R;
import com.ggchat.rgi.ggchat.database.GGChatDAO;
import com.ggchat.rgi.ggchat.model.Conversation;
import com.ggchat.rgi.ggchat.model.GroupInfoModel;
import com.ggchat.rgi.ggchat.navigation.Activity.ChatActivity;
import com.ggchat.rgi.ggchat.navigation.Fragments.ChatListFragment;
import com.ggchat.rgi.ggchat.singletones.GGChatHelper;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ChatMessageListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.tcp.sm.StreamManagementException;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.InvitationRejectionListener;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xevent.MessageEventManager;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by ti2 on 15/9/16.
 */
public class RoosterConnection implements ConnectionListener, ChatMessageListener, PingFailedListener {
    private static final String TAG = "RoosterConnection";

    private BroadcastReceiver uiThreadMessageReceiver;//Receives messages from the ui thread.

    private final Context mApplicationContext;
    private final String mUsername;
    private final String mPassword;
    private final String mServiceName;
    public static XMPPTCPConnection mConnection;
    private ChatMessageListener messageListener;
    public static int notificationId = 10;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder builder;
    private ChatData data;
    private static MessageEventManager mMessageEventManager;
    private ArrayList<GroupInfoModel> groupDetailsList = new ArrayList<GroupInfoModel>();

    @Override
    public void processMessage(Chat chat, Message message) {
//        Log.d(TAG, "---Received a message---");
//        Log.d(TAG, "message.getBody() :" + message.getBody());
//        Log.d(TAG, "message.getFrom() :" + message.getFrom());
//
//        String from = message.getFrom();
//        String contactJid = "";
//        if (from.contains("/")) {
//            contactJid = from.split("/")[0];
//            Log.d(TAG, "The real jid is :" + contactJid);
//        } else {
//            contactJid = from;
//        }
//
//        //Bundle up the intent and send the broadcast.
//        Intent intent = new Intent(RoosterConnectionService.NEW_MESSAGE);
//        intent.setPackage(mApplicationContext.getPackageName());
//        intent.putExtra(RoosterConnectionService.BUNDLE_FROM_JID, contactJid);
//        intent.putExtra(RoosterConnectionService.BUNDLE_MESSAGE_BODY, message.getBody());
//        mApplicationContext.sendBroadcast(intent);
//        Log.d(TAG, "Received message from :" + contactJid + " broadcast sent.");

    }

    @Override
    public void pingFailed() {
        Log.e("ping failed", "ok");
    }


    public static enum ConnectionState {
        CONNECTED, AUTHENTICATED, CONNECTING, DISCONNECTING, DISCONNECTED;
    }

    public static enum LoggedInState {
        LOGGED_IN, LOGGED_OUT;
    }


    public RoosterConnection(Context context) {
        Log.d(TAG, "RoosterConnection Constructor called.");

        mApplicationContext = context.getApplicationContext();
        data = new ChatData(mApplicationContext);
        data.open();

        String jid = PreferenceManager.getDefaultSharedPreferences(mApplicationContext)
                .getString("xmpp_jid", null);
        mPassword = PreferenceManager.getDefaultSharedPreferences(mApplicationContext)
                .getString("xmpp_password", null);

        if (jid != null) {
            mUsername = jid.split("@")[0];
            mServiceName = jid.split("@")[1];
        } else {
            mUsername = "";
            mServiceName = "";
        }
    }

    public void myConnect() throws IOException, XMPPException, SmackException {


        XMPPTCPConnectionConfiguration builder =
                XMPPTCPConnectionConfiguration.builder().
                        setServiceName(mServiceName).
                        setUsernameAndPassword(mUsername, mPassword).
                        setSecurityMode(ConnectionConfiguration.SecurityMode.disabled).
                        setDebuggerEnabled(true).
//        builder.setServiceName(SERVICE);
        setHost("chat-server.globalgarner.com").
                        setPort(5222).
                        setDebuggerEnabled(true).
                        build();


        mConnection = new XMPPTCPConnection(builder);
        mConnection.addConnectionListener(this);
        mConnection.connect();

//        try {
//            AccountManager accountManager = AccountManager.getInstance(mConnection);
//
//            boolean val = accountManager.supportsAccountCreation();
//            if (val) {
//                Log.e("creattion support", "ok");
//            } else {
//                Log.e("creattion support", "not ok");
//            }
//
//            accountManager.createAccount("rathod@globalgarner.com", "password");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        mConnection.login();
//
//        if (mConnection.isConnected()) {
//            getContactList();
//            //  registerRoster();
//        }
//
//        if (mConnection.isAuthenticated()) {
//            Log.w("app", "Auth done");
//            ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
//            chatManager.addChatListener(
//                    new ChatManagerListener() {
//                        @Override
//                        public void chatCreated(Chat chat, boolean createdLocally) {
//
//                            chat.addMessageListener(new ChatMessageListener() {
//                                @Override
//                                public void processMessage(Chat chat, Message message) {
//                                    System.out.println("Received message: "
//                                            + (message != null ? message.getBody() : "NULL"));
//
//                                    String from = message.getFrom();
//                                    String contactJid = "";
//                                    if (from.contains("/")) {
//                                        contactJid = from.split("/")[0];
//                                        Log.d(TAG, "The real jid is :" + contactJid);
//                                    } else {
//                                        contactJid = from;
//                                    }
//
//                                    //Bundle up the intent and send the broadcast.
//                                    Intent intent = new Intent(RoosterConnectionService.NEW_MESSAGE);
//                                    intent.setPackage(mApplicationContext.getPackageName());
//                                    intent.putExtra(RoosterConnectionService.BUNDLE_FROM_JID, contactJid);
//                                    intent.putExtra(RoosterConnectionService.BUNDLE_MESSAGE_BODY, message.getBody());
//                                    mApplicationContext.sendBroadcast(intent);
//                                }
//
//
//                            });
//
//                            Log.w("app", chat.toString());
//                        }
//                    });
//
//        }
    }


    public void connect() throws IOException, XMPPException, SmackException {
        myConnect();


//        Log.d(TAG, "Connecting to server " + mServiceName);
//        XMPPTCPConnectionConfiguration builder=
//                XMPPTCPConnectionConfiguration.builder().
//        setServiceName(mServiceName).
//        setUsernameAndPassword(mUsername, mPassword).
//        setSecurityMode(ConnectionConfiguration.SecurityMode.disabled).
////        builder.setServiceName(SERVICE);
//        setHost("globalgarner.com").
//        setPort(5222).
//        setDebuggerEnabled(true).
//        build();
//
//
//
//        mConnection = new XMPPTCPConnection(builder);
//        mConnection.addConnectionListener(this);
//        mConnection.connect();
//        mConnection.login();
//
//        if(mConnection.isConnected()){
//            getContactList();
//          //  registerRoster();
//        }
//
//        AbstractXMPPConnection conn1 = new XMPPTCPConnection(builder);
//        try {
//            conn1.connect();
//            if(conn1.isConnected())
//            {
//                Log.w("app", "conn done");
//            }
//
//            conn1.login();
//            if(conn1.isAuthenticated())
//            {
//                Log.w("app", "Auth done");
//                ChatManager chatManager = ChatManager.getInstanceFor(conn1);
//                chatManager.addChatListener(
//                        new ChatManagerListener() {
//                            @Override
//                            public void chatCreated(Chat chat, boolean createdLocally)
//                            {
//
//                                chat.addMessageListener(new ChatMessageListener()
//                                {
//                                    @Override
//                                    public void processMessage(Chat chat, Message message) {
//                                        System.out.println("Received message: "
//                                                + (message != null ? message.getBody() : "NULL"));
//
//                                    }
//
//
//                                });
//
//                                Log.w("app", chat.toString());
//                            }
//                        });
//
//            }
//        }
//        catch (Exception e) {
//            Log.w("app", e.toString());
//        }
//
//
//
//        messageListener = new ChatMessageListener() {
//            @Override
//            public void processMessage(Chat chat, Message message) {
//                ///ADDED
//                Log.e(TAG,"message.getBody() :"+message.getBody());
//                Log.e(TAG,"message.getFrom() :"+message.getFrom());
//
//                String from = message.getFrom();
//                String contactJid="";
//                if ( from.contains("/"))
//                {
//                    contactJid = from.split("/")[0];
//                    Log.d(TAG,"The real jid is :" +contactJid);
//                }else
//                {
//                    contactJid=from;
//                }
//
//                //Bundle up the intent and send the broadcast.
//                Intent intent = new Intent(RoosterConnectionService.NEW_MESSAGE);
//                intent.setPackage(mApplicationContext.getPackageName());
//                intent.putExtra(RoosterConnectionService.BUNDLE_FROM_JID,contactJid);
//                intent.putExtra(RoosterConnectionService.BUNDLE_MESSAGE_BODY,message.getBody());
//                mApplicationContext.sendBroadcast(intent);
//                Log.d(TAG,"Received message from :"+contactJid+" broadcast sent.");
//                ///ADDED
//
//            }
//        };
//
//        ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(mConnection);
//        reconnectionManager.setEnabledPerDefault(true);
//        reconnectionManager.enableAutomaticReconnection();

    }


    private List<Contact> getContactList() {
        List<Contact> mContacts = new ArrayList<>();

        mContacts.clear();
        Roster roster = mConnection.getRoster();
        Collection<RosterEntry> entries = roster.getEntries();
        Log.d("TRACE", "entries.size()=" + entries.size());
        for (RosterEntry e : entries) {
            Log.d("PRESENCE", e.getUser() + "=" + roster.getPresence(e.getUser()).isAvailable());
            mContacts.add(new Contact(e.getUser()));
            if (!data.isRosterInserted(e.getUser())) {
                data.insertRoster(e.getUser(), "nickname", "0");
            }
            if (roster.getPresence(e.getUser()).isAvailable()) {
                HashMap<String, String> contact = new HashMap<String, String>();
                contact.put("username", e.getName());
                contact.put("jid", e.getUser());
                Log.d("ADD", "NAME_KEY=" + e.getName() + " USERJID_KEY=" + e.getUser());
//                contacts.add(contact);
            }
        }
        data.close();
        return mContacts;
    }


    public void disconnect() {
        Log.d(TAG, "Disconnecting from serser " + mServiceName);
        try {
            if (mConnection != null) {
                mConnection.disconnect();
            }

        } catch (SmackException.NotConnectedException e) {
            RoosterConnectionService.sConnectionState = ConnectionState.DISCONNECTED;
            e.printStackTrace();

        }
        mConnection = null;

    }


    @Override
    public void connected(XMPPConnection connection) {
        RoosterConnectionService.sConnectionState = ConnectionState.CONNECTED;
        PingManager.getInstanceFor(mConnection).setDefaultPingInterval(2);
        try {

            mConnection.login();
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        getContactList();
//        setServiceDiscovery();
        Log.d(TAG, "Connected Successfully");
    }


    private void createGroupChat() {
        // Get the MultiUserChatManager
        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(mConnection);

// Create a MultiUserChat using an XMPPConnection for a room
        MultiUserChat muc2 = manager.getMultiUserChat("ggroup@conference.globalgarner.com");


        try {
            muc2.join("testbot2");
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
        // User2 listens for invitation rejections
        muc2.addInvitationRejectionListener(new InvitationRejectionListener() {
            public void invitationDeclined(String invitee, String reason) {
                // Do whatever you need here...
                Log.e("invite declined", reason);
            }
        });
        // User2 invites user3 to join to the room
        try {
            muc2.invite("jay@globalgarner.com/Smack", "Meet me in this excellent room");
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
//
//        // Get the MultiUserChatManager
//        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(mConnection);
//
//        // Get a MultiUserChat using MultiUserChatManager
//        MultiUserChat muc = manager.getMultiUserChat("ggroup@conference.globalgarner.com");
//
//        // Create the room
//        try {
//            muc.create("testbot");
//        } catch (XMPPException.XMPPErrorException e) {
//            e.printStackTrace();
//        } catch (SmackException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void authenticated(XMPPConnection connection) {
        RoosterConnectionService.sConnectionState = ConnectionState.CONNECTED;
        mApplicationContext.startService(new Intent(mApplicationContext, AuthenticatorService.class));

        Log.d(TAG, "Authenticated Successfully");
//        createGroupChat();

        setPresenceSubscription();
        PresenceHandler.publishPresenceWhenConnecting(mApplicationContext);

        ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
        chatManager.addChatListener(
                new ChatManagerListener() {
                    @Override
                    public void chatCreated(Chat chat, boolean createdLocally) {

                        chat.addMessageListener(new ChatMessageListener() {
                            @Override
                            public void processMessage(Chat chat, Message message) {

                                System.out.println("Received message: "
                                        + (message != null ? message.getBody() : "NULL"));

                                String from = message.getFrom();
                                String contactJid = "";
                                if (from.contains("/")) {
                                    contactJid = from.split("/")[0];
                                    Log.d(TAG, "The real jid is :" + contactJid);
                                } else {
                                    contactJid = from;
                                }

                                Log.e("packaet id", message.getPacketID() + "");


                                if (message.getBodies().size() == 0) {

                                } else {
                                    if (message.getPacketID() != null) {
                                        Set<Message.Body> bodies = message.getBodies();
                                        String msg = "";

                                        for (Message.Body body : bodies) {
                                            msg = body.getMessage();
                                            break;
                                        }
                                        if (GGChatHelper.getInstance().isChatActivityRunning()) {
                                            //Bundle up the intent and send the broadcast.
                                            GGChatDAO.insertChatHistory(msg, contactJid, "1", "2", getTimeStamp());
                                            Intent intent = new Intent(RoosterConnectionService.NEW_MESSAGE);
                                            intent.setPackage(mApplicationContext.getPackageName());
                                            intent.putExtra(RoosterConnectionService.BUNDLE_FROM_JID, contactJid);

                                            intent.putExtra(RoosterConnectionService.BUNDLE_MESSAGE_BODY, msg);
                                            mApplicationContext.sendBroadcast(intent);
                                        } else if (ChatListFragment.isRunning) {
                                            try {
                                                mConnection.sendSmAcknowledgement();
                                            } catch (StreamManagementException.StreamManagementNotEnabledException e) {
                                                e.printStackTrace();
                                            } catch (SmackException.NotConnectedException e) {
                                                e.printStackTrace();
                                            }

                                            GGChatDAO.insertChatHistory(msg, contactJid, "0", "2", getTimeStamp());
                                            EventBus.getDefault().post(new Conversation());
                                        } else {
                                            try {
                                                mConnection.sendSmAcknowledgement();
                                            } catch (StreamManagementException.StreamManagementNotEnabledException e) {
                                                e.printStackTrace();
                                            } catch (SmackException.NotConnectedException e) {
                                                e.printStackTrace();
                                            }

                                            GGChatDAO.insertChatHistory(msg, contactJid, "0", "2", getTimeStamp());
                                            sendNotification(msg, contactJid);
                                        }
                                    }
                                }

                            }


                        });

                        Log.w("app", chat.toString());
                    }
                });
        showContactListActivityWhenAuthenticated();
    }


    @Override
    public void connectionClosed() {
        RoosterConnectionService.sConnectionState = ConnectionState.DISCONNECTED;
        Log.d(TAG, "Connectionclosed()");

    }

    @Override
    public void connectionClosedOnError(Exception e) {
        RoosterConnectionService.sConnectionState = ConnectionState.DISCONNECTED;
        Log.d(TAG, "ConnectionClosedOnError, error " + e.toString());

    }

    @Override
    public void reconnectingIn(int seconds) {
        RoosterConnectionService.sConnectionState = ConnectionState.CONNECTING;
        Log.d(TAG, "ReconnectingIn() ");

    }

    @Override
    public void reconnectionSuccessful() {
        RoosterConnectionService.sConnectionState = ConnectionState.CONNECTED;
        Log.d(TAG, "ReconnectionSuccessful()");

    }

    @Override
    public void reconnectionFailed(Exception e) {
        RoosterConnectionService.sConnectionState = ConnectionState.DISCONNECTED;
        Log.d(TAG, "ReconnectionFailed()");

    }

    private void showContactListActivityWhenAuthenticated() {
        Intent i = new Intent(RoosterConnectionService.UI_AUTHENTICATED);
        i.setPackage(mApplicationContext.getPackageName());
        mApplicationContext.sendBroadcast(i);
        Log.d(TAG, "Sent the broadcast that we are authenticated");
    }

    private void setupUiThreadBroadCastMessageReceiver() {
        uiThreadMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Check if the Intents purpose is to send the message.
                String action = intent.getAction();
                if (action.equals(RoosterConnectionService.SEND_MESSAGE)) {
                    //SENDS THE ACTUAL MESSAGE TO THE SERVER
                    sendMessage(intent.getStringExtra(RoosterConnectionService.BUNDLE_MESSAGE_BODY),
                            intent.getStringExtra(RoosterConnectionService.BUNDLE_TO));
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(RoosterConnectionService.SEND_MESSAGE);
        mApplicationContext.registerReceiver(uiThreadMessageReceiver, filter);

    }

    public void sendMessage(String body, String toJid) {
        Log.d(TAG, "Sending message to :" + toJid);
        Chat chat = ChatManager.getInstanceFor(mConnection)
                .createChat(toJid, this);
        try {
            Message message = new Message();
            message.addBody("EN", body);
            String deliveryReceiptId = DeliveryReceiptRequest.addTo(message);
            chat.sendMessage(body);
//            Log.e("sendMessage: deliveryReceiptId for this message is: " + deliveryReceiptId, "ok");
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(Message message, String toJid) {
        Chat chat = ChatManager.getInstanceFor(mConnection)
                .createChat(toJid, this);
        try {
            chat.sendMessage(message);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }


    private SSLContext createSSLContext(Context context) throws KeyStoreException,
            NoSuchAlgorithmException, KeyManagementException, IOException, CertificateException {

        KeyStore trustStore;
        InputStream in = null;
        trustStore = KeyStore.getInstance("BKS");

        in = context.getResources().openRawResource(R.raw.gg_certificate);

        trustStore.load(in, "rgi123".toCharArray());

        TrustManagerFactory trustManagerFactory = TrustManagerFactory
                .getInstance(KeyManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(trustStore);
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
        return sslContext;
    }

    public boolean createGroup(String groupName) {
        if (mConnection == null)
            return false;
        try {
            mConnection.getRoster().createGroup(groupName);
            Log.v("Group created : ", groupName);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void sendNotification(String msg, String jid) {

        mNotificationManager = (NotificationManager) mApplicationContext.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(mApplicationContext, ChatActivity.class);
        intent.putExtra("jid", jid);

        PendingIntent contentIntent = PendingIntent.getActivity(mApplicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mApplicationContext).setSmallIcon(R.mipmap.gg_logo)
                .setContentTitle(mApplicationContext.getString(R.string.app_name))
                .setContentText(msg)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg)).setContentText(msg);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(uri);
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setAutoCancel(true);
        mNotificationManager.notify(notificationId, mBuilder.build());
    }

    private String getTimeStamp() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
        return sdf.format(date);
    }

    private void setServiceDiscovery() {
        groupDetailsList.clear();

        try {
            ServiceDiscoveryManager discoManager = ServiceDiscoveryManager.getInstanceFor(RoosterConnection.mConnection);
            DiscoverItems items = null;
            try {
                items = discoManager.discoverItems("chat-server.globalgarner.com");
            } catch (SmackException.NoResponseException e) {
                e.printStackTrace();
            } catch (XMPPException.XMPPErrorException e) {
                e.printStackTrace();
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }


            List<DiscoverItems.Item> listIt = items.getItems();
            for (DiscoverItems.Item it : listIt) {
                DiscoverItems.Item item = it;
                String occupant = item.getEntityID().toString();
                Log.e("occupant", occupant);
//            occupant = occupant.split("/")[1];
                GroupInfoModel groupInfoModel = new GroupInfoModel();
                groupInfoModel.setAdmin(false);
                groupInfoModel.setMemberId(occupant + "@" + "globalgarner.com");
//            groupInfoModel.setMemberName(occupant);
                groupDetailsList.add(groupInfoModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static MessageEventManager getMessageEventInstance() {
        return mMessageEventManager;
    }

    private void setPresenceSubscription() {
        // presence
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mApplicationContext);

            Presence pres = new Presence(Presence.Type.subscribe);

            pres.setFrom(prefs.getString("xmpp_jid", ""));
            RoosterConnection.mConnection.sendStanzaWithResponseCallback(pres, new PacketFilter() {
                @Override
                public boolean accept(Packet packet) {
                    return false;
                }
            }, new PacketListener() {
                @Override
                public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
