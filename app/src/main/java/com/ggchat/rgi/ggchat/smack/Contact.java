package com.ggchat.rgi.ggchat.smack;

/**
 * Created by ti2 on 15/9/16.
 */
public class Contact {
    private String jid;

    public Contact(String contactJid )
    {
        jid = contactJid;
    }

    public String getJid()
    {
        return jid;
    }

    public void setJid(String jid) {
        this.jid = jid;
    }
}
