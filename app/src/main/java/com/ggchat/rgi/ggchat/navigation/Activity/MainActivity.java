package com.ggchat.rgi.ggchat.navigation.Activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.ggchat.rgi.ggchat.R;
import com.ggchat.rgi.ggchat.constant.Constant;
import com.ggchat.rgi.ggchat.database.GGChatDAO;
import com.ggchat.rgi.ggchat.database.SharedPreferenceUtil;
import com.ggchat.rgi.ggchat.model.Conversation;
import com.ggchat.rgi.ggchat.model.TabChange;
import com.ggchat.rgi.ggchat.navigation.Fragments.ChatListFragment;
import com.ggchat.rgi.ggchat.navigation.Fragments.ContactListFragment;
import com.ggchat.rgi.ggchat.smack.Availability;
import com.ggchat.rgi.ggchat.smack.PresenceHandler;
import com.ggchat.rgi.ggchat.smack.RoosterConnection;
import com.ggchat.rgi.ggchat.smack.RoosterConnectionService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    String TAG = "gg_tag";
    private boolean debug = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PresenceHandler.publishPresenceFromForeground(this);
        init();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        setSupportActionBar(toolbar);

        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().equals(getString(R.string.chats))) {
                    EventBus.getDefault().post(new Conversation());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.e("tab re selected", "ok");
            }
        });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private String[] titles = new String[]{getString(R.string.chats), getString(R.string.contacts)};

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;

            if (position == 0) {
                fragment = new ChatListFragment();
            } else if (position == 1) {
                fragment = new ContactListFragment();
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Conversation conversation) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.actionNewGroup:
                startActivity(new Intent(MainActivity.this, GroupChatInviteActivity.class));
                break;
        }
        return true;
    }
}
