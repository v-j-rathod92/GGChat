package com.ggchat.rgi.ggchat.navigation.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ggchat.rgi.ggchat.R;
import com.ggchat.rgi.ggchat.adapter.ContactListAdapter;
import com.ggchat.rgi.ggchat.listeners.OnRecyclerClickListener;
import com.ggchat.rgi.ggchat.model.Conversation;
import com.ggchat.rgi.ggchat.navigation.Activity.ChatActivity;
import com.ggchat.rgi.ggchat.smack.ChatData;
import com.ggchat.rgi.ggchat.smack.Contact;

import java.util.List;

/**
 * Created by rgi-40 on 25/3/17.
 */

public class ContactListFragment extends Fragment {
    private View rootView;
    private RecyclerView recyclerViewChatList;
    private ChatData data;
    private List<Contact> contacts;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_contact_list, container, false);

        init();

        return rootView;
    }

    private void init(){

        recyclerViewChatList = (RecyclerView) rootView.findViewById(R.id.recyclerViewChatList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewChatList.setLayoutManager(mLayoutManager);
        recyclerViewChatList.setItemAnimator(new DefaultItemAnimator());

        data = new ChatData(getActivity());
        data.open();

        contacts = data.getAllRosterList();
        recyclerViewChatList.setAdapter(new ContactListAdapter(getActivity(), contacts, new OnRecyclerClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(getActivity(), ChatActivity.class);
                intent.putExtra("jid", contacts.get(position).getJid());
                startActivity(intent);
            }
        }));
    }

}
