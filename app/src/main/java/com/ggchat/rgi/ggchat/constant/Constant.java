package com.ggchat.rgi.ggchat.constant;

/**
 * Created by rgi-40 on 7/4/17.
 */

public class Constant {
    public static final String IS_PRESENCE_PUBLISHED = "is_presence_sent";
    public static final String IS_REQUEST_PENDING_FOR_PUBLISH_PRESENCE = "is_presence_sent";
}
