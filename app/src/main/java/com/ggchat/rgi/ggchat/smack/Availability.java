package com.ggchat.rgi.ggchat.smack;

import android.content.Context;

import com.ggchat.rgi.ggchat.R;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

/**
 * Created Vishal Rathod on 5/4/17.
 */

public class Availability {

    public enum Status {
        CHAT, ONLINE, AWAY, XA, DND, OFFLINE;

        public String toShowString() {
            switch (this) {
                case ONLINE:
                    return "online";
                case CHAT:
                    return "chat";
                case AWAY:
                    return "away";
                case XA:
                    return "xa";
                case DND:
                    return "dnd";
                case OFFLINE:
                    return "offline";
            }
            return null;
        }
    }

    public static String retrievePresence(Presence.Mode userMode, boolean isOnline) {
        if (userMode == Presence.Mode.dnd) {
            return Availability.Status.DND.toString();
        } else if (userMode == Presence.Mode.away || userMode == Presence.Mode.xa) {
            return Availability.Status.AWAY.toString();
        } else if (isOnline) {
            return Availability.Status.ONLINE.toString();
        } else {
            return Availability.Status.OFFLINE.toString();
        }
    }

    public static void sendPresence(Context mContext) {
        Presence p = new Presence(Presence.Type.available);
        p.setMode(Presence.Mode.available);
        p.setPacketID(mContext.getString(R.string.presence_packet_id));
        try {
            RoosterConnection.mConnection.sendStanzaWithResponseCallback(p, new PacketFilter() {
                @Override
                public boolean accept(Packet packet) {
                    return true;
                }
            }, new PacketListener() {
                @Override
                public void processPacket(Packet packet) throws SmackException.NotConnectedException {

                }
            });
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }
}
