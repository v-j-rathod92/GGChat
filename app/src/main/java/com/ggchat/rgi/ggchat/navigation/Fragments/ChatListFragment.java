package com.ggchat.rgi.ggchat.navigation.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ggchat.rgi.ggchat.R;
import com.ggchat.rgi.ggchat.adapter.ChatListAdapter;
import com.ggchat.rgi.ggchat.database.GGChatDAO;
import com.ggchat.rgi.ggchat.listeners.OnRecyclerClickListener;
import com.ggchat.rgi.ggchat.model.Conversation;
import com.ggchat.rgi.ggchat.navigation.Activity.GroupChatInviteActivity;
import com.ggchat.rgi.ggchat.smack.ChatData;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rgi-40 on 25/3/17.
 */

public class ChatListFragment extends Fragment {
    private View rootView;
    private RecyclerView recyclerViewChatList;
    private ChatData data;
    private List<Conversation> listChat = new ArrayList<>();
    private ChatListAdapter adapter;
    public static boolean isRunning = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_chat_list, container, false);

        init();

        return rootView;
    }

    private void init() {

        recyclerViewChatList = (RecyclerView) rootView.findViewById(R.id.recyclerViewChatList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewChatList.setLayoutManager(mLayoutManager);
        recyclerViewChatList.setItemAnimator(new DefaultItemAnimator());

        data = new ChatData(getActivity());
        data.open();

        adapter = new ChatListAdapter(getActivity(), listChat, new OnRecyclerClickListener() {
            @Override
            public void onClick(int position) {
//                Intent intent = new Intent(getActivity(), ChatActivity.class);
//                intent.putExtra("jid", listChat.get(position).getWithUserJid());
//                startActivity(intent);

                Intent intent = new Intent(getActivity(), GroupChatInviteActivity.class);
                startActivity(intent);
            }
        });

        recyclerViewChatList.setAdapter(adapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Conversation conversation) {
        /**
         * Either call by tab change or new message arrive
         */
        updateChatList();
    }

    @Override
    public void onResume() {
        super.onResume();

        isRunning = true;
        updateChatList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        isRunning = false;
        EventBus.getDefault().unregister(this);
    }

    private void updateChatList(){
        listChat.clear();
        listChat.addAll(GGChatDAO.getRecentChats());
        adapter.notifyDataSetChanged();
    }
}
