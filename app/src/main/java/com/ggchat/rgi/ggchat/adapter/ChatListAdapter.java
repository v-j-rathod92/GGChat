package com.ggchat.rgi.ggchat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ggchat.rgi.ggchat.R;
import com.ggchat.rgi.ggchat.database.GGChatDAO;
import com.ggchat.rgi.ggchat.listeners.OnRecyclerClickListener;
import com.ggchat.rgi.ggchat.model.Conversation;
import com.ggchat.rgi.ggchat.smack.Contact;

import java.util.List;

/**
 * Created by rgi-40 on 27/3/17.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder>{
    private Context mContext;
    private List<Conversation> listConversation;
    private OnRecyclerClickListener onRecyclerClick;

    public ChatListAdapter(Context mContext, List<Conversation> listConversation, OnRecyclerClickListener onRecyclerClick) {
        this.mContext = mContext;
        this.listConversation = listConversation;
        this.onRecyclerClick = onRecyclerClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_chat_list_item, null);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.txtUsername.setText(listConversation.get(position).getWithUserJid().split("@")[0]);
        holder.txtMessage.setText(listConversation.get(position).getMessage());

        if(listConversation.get(position).getType().endsWith("2") &&
                listConversation.get(position).getIsSeen().equals("0")){
            holder.txtTotalMessages.setVisibility(View.VISIBLE);
            int totalUnread = GGChatDAO.getTotalUnreadMessage(listConversation.get(position).getWithUserJid());

            holder.txtTotalMessages.setText(String.valueOf(totalUnread));
        }
        else{
            holder.txtTotalMessages.setVisibility(View.INVISIBLE);
        }

        holder.linearLayoutContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerClick.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listConversation.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayoutContact;
        TextView txtUsername, txtDate, txtMessage, txtTotalMessages;

        public ViewHolder(View itemView) {
            super(itemView);

            linearLayoutContact = (LinearLayout) itemView.findViewById(R.id.linearLayoutContact);
            txtUsername = (TextView) itemView.findViewById(R.id.txtUsername);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtMessage = (TextView) itemView.findViewById(R.id.txtMessage);
            txtTotalMessages = (TextView) itemView.findViewById(R.id.txtTotalMessages);
        }
    }
}
