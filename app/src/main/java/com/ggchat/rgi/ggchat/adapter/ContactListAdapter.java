package com.ggchat.rgi.ggchat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ggchat.rgi.ggchat.R;
import com.ggchat.rgi.ggchat.listeners.OnRecyclerClickListener;
import com.ggchat.rgi.ggchat.smack.Contact;

import java.util.List;

/**
 * Created by rgi-40 on 25/3/17.
 */

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder>{

    private Context mContext;
    private List<Contact> listUsers;
    private OnRecyclerClickListener onRecyclerClick;

    public ContactListAdapter(Context mContext, List<Contact> listUsers, OnRecyclerClickListener onRecyclerClick) {
        this.mContext = mContext;
        this.listUsers = listUsers;
        this.onRecyclerClick = onRecyclerClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_contact_list_item, null);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.txtUsername.setText(listUsers.get(position).getJid().split("@")[0]);

        holder.linearLayoutContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerClick.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listUsers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayoutContact;
        TextView txtUsername, txtMessage;

        public ViewHolder(View itemView) {
            super(itemView);

            linearLayoutContact = (LinearLayout) itemView.findViewById(R.id.linearLayoutContact);
            txtUsername = (TextView) itemView.findViewById(R.id.txtUsername);
            txtMessage = (TextView) itemView.findViewById(R.id.txtMessage);
        }
    }
}
