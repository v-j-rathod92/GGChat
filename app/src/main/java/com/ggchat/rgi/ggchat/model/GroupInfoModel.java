package com.ggchat.rgi.ggchat.model;

import android.support.annotation.NonNull;

import com.ggchat.rgi.ggchat.navigation.Activity.ChatActivity;

import java.io.Serializable;

/**
 * Created by rgi-40 on 29/3/17.
 */

public class GroupInfoModel implements Comparable<GroupInfoModel>, Serializable {
    private static final long serialVersionUID = 1L;
    private String memberId = "", memberName = "";
    private boolean isAdmin;
    public String getMemberId() {
        return memberId;
    }
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
    public String getMemberName() {
        return memberName;
    }
    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }
    public boolean isAdmin() {
        return isAdmin;
    }
    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public int compareTo(@NonNull GroupInfoModel another) {
        return getMemberName().compareTo(another.getMemberName());
    }
}
