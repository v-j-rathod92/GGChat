package com.ggchat.rgi.ggchat.smack;

import android.content.Context;
import android.util.Log;

import com.ggchat.rgi.ggchat.constant.Constant;
import com.ggchat.rgi.ggchat.database.SharedPreferenceUtil;
import com.ggchat.rgi.ggchat.util.CommonUtil;

/**
 * Created by Vishal Rathod on 7/4/17.
 */

public class PresenceHandler {

    public static void publishPresenceWhenConnecting(Context mContext){
        if(CommonUtil.checkIfAppIsOnForeground(mContext)){
            Availability.sendPresence(mContext);
            SharedPreferenceUtil.putValue(Constant.IS_PRESENCE_PUBLISHED, true);
        }
        else{
            SharedPreferenceUtil.putValue(Constant.IS_PRESENCE_PUBLISHED, false);
        }

        if(SharedPreferenceUtil.getBoolean(Constant.IS_REQUEST_PENDING_FOR_PUBLISH_PRESENCE, false)){
            SharedPreferenceUtil.putValue(Constant.IS_REQUEST_PENDING_FOR_PUBLISH_PRESENCE, false);
            Availability.sendPresence(mContext);
        }
    }

    public static void publishPresenceFromForeground(Context mContext){
        if(!SharedPreferenceUtil.getBoolean(Constant.IS_PRESENCE_PUBLISHED, false)){
            if (RoosterConnectionService.getState().equals(RoosterConnection.ConnectionState.CONNECTED)) {
                Log.e("connected presence send", "ok");
                Availability.sendPresence(mContext);
            }
            else{
                Log.e("not connected presence pending", "ok");
                SharedPreferenceUtil.putValue(Constant.IS_REQUEST_PENDING_FOR_PUBLISH_PRESENCE, true);
            }
        }
    }
}
