package com.ggchat.rgi.ggchat.model;

import com.ggchat.rgi.ggchat.constant.Status;

/**
 * Created by rgi-40 on 29/3/17.
 */

public class MessageStatus {
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
