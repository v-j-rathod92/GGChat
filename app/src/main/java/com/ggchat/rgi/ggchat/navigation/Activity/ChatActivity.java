package com.ggchat.rgi.ggchat.navigation.Activity;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import com.ggchat.rgi.ggchat.R;
import com.ggchat.rgi.ggchat.constant.Status;
import com.ggchat.rgi.ggchat.database.GGChatDAO;
import com.ggchat.rgi.ggchat.model.Conversation;
import com.ggchat.rgi.ggchat.model.MessageStatus;
import com.ggchat.rgi.ggchat.singletones.GGChatHelper;
import com.ggchat.rgi.ggchat.smack.Availability;
import com.ggchat.rgi.ggchat.smack.ChatData;
import com.ggchat.rgi.ggchat.smack.RoosterConnection;
import com.ggchat.rgi.ggchat.smack.RoosterConnectionService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jivesoftware.smackx.xevent.MessageEventManager;
import org.jivesoftware.smackx.xevent.MessageEventNotificationListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;

/**
 * Created by rgi-40 on 25/3/17.
 */

public class ChatActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private String contactJid;
    private ChatView mChatView;
    private BroadcastReceiver mBroadcastReceiver;
    private RoosterConnection rr;
    private TextView txtTitle, txtStatus;
    private Handler handler = new Handler();
    private MessageEventManager mMessageEventManager;
    private static final String TAG = "ChatActivity_Log";
    private boolean isUserIsOnline;
    private Roster roster;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_chat_screen);

        init();
        setListener();
    }

    private void init() {
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtStatus = (TextView) findViewById(R.id.txtStatus);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        contactJid = getIntent().getStringExtra("jid");

        setSupportActionBar(toolbar);

        mChatView = (ChatView) findViewById(R.id.chatView);
        rr = new RoosterConnection(ChatActivity.this);
        mMessageEventManager = RoosterConnection.getMessageEventInstance();
        txtTitle.setText(contactJid.split("@")[0]);

        roster = RoosterConnection.mConnection.getRoster();

        getAvailability();
        setAvailability();
        setChatHistory();
        setAllReceivedMessageAsRead();
        chatStateRecognizer();

        ProviderManager.addExtensionProvider(DeliveryReceipt.ELEMENT, DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
        ProviderManager.addExtensionProvider(DeliveryReceiptRequest.ELEMENT, new DeliveryReceiptRequest().getNamespace(), new DeliveryReceiptRequest.Provider());

        DeliveryReceiptManager dm = DeliveryReceiptManager.getInstanceFor(RoosterConnection.mConnection);
        dm.setAutoReceiptsEnabled(true);

        DeliveryReceiptManager.getInstanceFor(RoosterConnection.mConnection).addReceiptReceivedListener(new ReceiptReceivedListener() {
            @Override
            public void onReceiptReceived(String fromJid, String toJid, final String receiptId, final Packet receipt) {
                Log.e("onReceiptReceived: from: " + fromJid + " to: " + toJid + " deliveryReceiptId: " + receiptId + " packet id: " + receipt.getPacketID(), "ok");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        mChatView.setMessageDelivery(receiptId, ChatMessage.DeliveryStatus.SENT);
                    }
                });
            }
        });
    }


    private void chatStateRecognizer() {

        roster.addRosterListener(new RosterListener() {
            @Override
            public void entriesAdded(Collection<String> addresses) {
                Log.e(TAG, "entries added : " + addresses.toString());

            }

            @Override
            public void entriesUpdated(Collection<String> addresses) {
                Log.e(TAG, "entries udpated : " + addresses.toString());
            }

            @Override
            public void entriesDeleted(Collection<String> addresses) {
                Log.e(TAG, "entries deleted : " + addresses.toString());
            }

            @Override
            public void presenceChanged(Presence presence) {
//                Log.e(TAG, "presence changed : " + Availability.retrievePresence(presence.getMode(), presence.isAvailable()));
                roster = RoosterConnection.mConnection.getRoster();
                if(presence.getPacketID() == null){
                    Log.e("packet id", "null");
                }
                else{
                    Log.e("packet id", presence.getPacketID());
                }

                String user = presence.getFrom();
                Presence mPresence = roster.getPresence(user);
                String packetId =  mPresence.getPacketID();

                if(packetId == null){
                    Log.e("is available", "not ok");
                    isUserIsOnline = false;
                }
                else if(mPresence.isAvailable() && packetId.equals(getString(R.string.presence_packet_id))){
                    Log.e("is available", "ok");
                    isUserIsOnline = true;
                }
                else{
                    isUserIsOnline = false;
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        setAvailability();
                    }
                });
            }
        });




        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                mMessageEventManager = MessageEventManager.getInstanceFor(RoosterConnection.mConnection);
                mMessageEventManager.addMessageEventNotificationListener(new MessageEventNotificationListener() {
                    @Override
                    public void offlineNotification(String arg0, String arg1) {
                        Log.e("offline notif", "ok");
                    }

                    @Override
                    public void displayedNotification(String arg0, String arg1) {
                        Log.e("display notif", "ok");
                    }

                    @Override
                    public void deliveredNotification(String from, String arg1) {
                        Log.e("delivered notif", "ok");
                    }

                    @Override
                    public void composingNotification(String from, String to) {
                        Log.i("Receiver:Compose state", from + " is started typing......" + to);

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                txtStatus.setText("typing...");
                            }
                        });
                    }

                    @Override
                    public void cancelledNotification(String from, String to) {
                        Log.i("Receiver:Stop state", from + " is stopped typing......" + to);

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                txtStatus.setText("Online");
                            }
                        });
                    }

                });
            }
        });

        thread.start();
    }

    private void setListener() {
        mChatView.setTypingListener(new ChatView.TypingListener() {
            @Override
            public void userStartedTyping() {
                Log.e("typeing", "typing...");

                try {
                    mMessageEventManager.sendComposingNotification(contactJid, "2");
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void userStoppedTyping() {
                Log.e("user stopped typing", "ok");
                try {
                    mMessageEventManager.sendCancelledNotification(contactJid, "2");
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        });

        mChatView.setOnSentMessageListener(new ChatView.OnSentMessageListener() {
            @Override
            public boolean sendMessage(ChatMessage chatMessage) {

                if (RoosterConnectionService.getState().equals(RoosterConnection.ConnectionState.CONNECTED)) {
                    Log.d("TAG", "The client is connected to the server,Sendint Message");

                    //Send the message to the server
                    Intent intent = new Intent(RoosterConnectionService.SEND_MESSAGE);
                    intent.putExtra(RoosterConnectionService.BUNDLE_MESSAGE_BODY,
                            mChatView.getTypedMessage());
                    intent.putExtra(RoosterConnectionService.BUNDLE_TO, contactJid);
                    sendBroadcast(intent);


                    Message message = new Message();
                    message.addBody("EN", mChatView.getTypedMessage());
                    String deliveryReceiptId = DeliveryReceiptRequest.addTo(message);
                    chatMessage.setReceiptId(deliveryReceiptId);
                    rr.sendMessage(message, contactJid);

                    Log.e("sendMessage: deliveryReceiptId for this message is: " + deliveryReceiptId, "ok");

                    rr.createGroup("group1");
                    GGChatDAO.insertChatHistory(mChatView.getTypedMessage(), contactJid, "1", "1", getTimeStamp());
                    return true;
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Client not connected to server ,Message not sent!",
                            Toast.LENGTH_LONG).show();

                    return false;
                }
            }
        });
    }


    private String getTimeStamp() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
        return sdf.format(date);
    }

    @Override
    protected void onResume() {
        super.onResume();
        GGChatHelper.getInstance().setChatActivityRunning(true);

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case RoosterConnectionService.NEW_MESSAGE:
                        String from = intent.getStringExtra(RoosterConnectionService.BUNDLE_FROM_JID);
                        String body = intent.getStringExtra(RoosterConnectionService.BUNDLE_MESSAGE_BODY);

                        if (from.equals(contactJid)) {
                            ChatMessage chatMessage = new ChatMessage(body, System.currentTimeMillis(), ChatMessage.Type.RECEIVED);
                            mChatView.addMessage(chatMessage);
                        } else {
                            Log.d("TAG", "Got a message from jid :" + from);
                        }

                        return;
                }

            }
        };

        IntentFilter filter = new IntentFilter(RoosterConnectionService.NEW_MESSAGE);
        registerReceiver(mBroadcastReceiver, filter);
    }


    @Override
    protected void onPause() {
        super.onPause();

        GGChatHelper.getInstance().setChatActivityRunning(false);
        unregisterReceiver(mBroadcastReceiver);
    }

    private void setChatHistory() {

        List<Conversation> listChatHistory = GGChatDAO.getConversation(contactJid);

        for (Conversation conversation : listChatHistory) {
            String type = conversation.getType();
            if (type.endsWith("1")) {
                ChatMessage chatMessage = new ChatMessage(conversation.getMessage(), getTime(conversation.getDate())
                        , ChatMessage.Type.SENT);

                mChatView.addMessage(chatMessage);
            } else {
//                mChatView.receiveMessage(conversation.getMessage());
                ChatMessage chatMessage = new ChatMessage(conversation.getMessage(), getTime(conversation.getDate())
                        , ChatMessage.Type.RECEIVED);

                mChatView.addMessage(chatMessage);
            }
        }
    }


    private void setAllReceivedMessageAsRead() {
        GGChatDAO.setAllReceivedMessageRead(contactJid);
    }

    private long getTime(String conversationDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
        Date date = null;
        try {
            date = sdf.parse(conversationDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageStatus messageStatus) {
        Log.e("on event", "ok");

        if (messageStatus.getStatus() == Status.TYPING) {
            txtStatus.setText("typing...");
        } else if (messageStatus.getStatus() == Status.TYPING_STOP) {
            txtStatus.setText("Online");
        }
    }

    private void getAvailability(){
        Presence availability = roster.getPresence(contactJid);
        if(availability.isAvailable()){
            isUserIsOnline = true;
        }
        else{
            isUserIsOnline = false;
        }
    }

    private void setAvailability(){
        if(isUserIsOnline){
            txtStatus.setVisibility(View.VISIBLE);
            txtStatus.setText(getString(R.string.online));
        }
        else{
            txtStatus.setVisibility(View.GONE);
        }
    }

}
