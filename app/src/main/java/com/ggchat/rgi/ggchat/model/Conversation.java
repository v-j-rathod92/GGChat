package com.ggchat.rgi.ggchat.model;

/**
 * Created by rgi-40 on 27/3/17.
 */

public class Conversation {
    String id, message, jid, withUserJid, isSeen, type, date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getJid() {
        return jid;
    }

    public void setJid(String jid) {
        this.jid = jid;
    }

    public String getWithUserJid() {
        return withUserJid;
    }

    public void setWithUserJid(String withUserJid) {
        this.withUserJid = withUserJid;
    }

    public String getIsSeen() {
        return isSeen;
    }

    public void setIsSeen(String isSeen) {
        this.isSeen = isSeen;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
