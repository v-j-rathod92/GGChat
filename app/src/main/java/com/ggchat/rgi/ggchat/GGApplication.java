package com.ggchat.rgi.ggchat;

import android.app.Application;

import com.ggchat.rgi.ggchat.database.DatabaseUtil;
import com.ggchat.rgi.ggchat.database.SharedPreferenceUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by rgi-40 on 27/3/17.
 */

public class GGApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseUtil.init(this, "GGChatDatabase", 1, null);
        SharedPreferenceUtil.init(this);
    }
}
