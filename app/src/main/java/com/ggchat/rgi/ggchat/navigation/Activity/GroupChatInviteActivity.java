package com.ggchat.rgi.ggchat.navigation.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ggchat.rgi.ggchat.R;
import com.ggchat.rgi.ggchat.smack.RoosterConnection;

import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.InvitationRejectionListener;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;

/**
 * Created by Vishal Rathod on 12/4/17.
 */

public class GroupChatInviteActivity extends AppCompatActivity {
    ;
    private EditText editText;
    private Button btnInvite, btnRoom;
    private MultiUserChatManager managerSendInvitation;
    private MultiUserChatManager managerreceiveInvitation;
    private MultiUserChat muc2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_group_chat_invite);

        init();
        setListener();
    }

    private void init() {

        editText = (EditText) findViewById(R.id.edtText);
        btnInvite = (Button) findViewById(R.id.btnInvite);
        btnRoom = (Button) findViewById(R.id.btnRoom);

        managerSendInvitation = MultiUserChatManager.getInstanceFor(RoosterConnection.mConnection);
    }

    private void setListener() {
        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInvitation();
            }
        });

        btnRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupChatInviteActivity.this, GroupChatActivity.class);
                startActivity(intent);
                finish();
            }
        });


        invitationListener();
    }

    private void sendInvitation() {

        muc2 = managerSendInvitation.getMultiUserChat("ggchat@conference.globalgarner.com");

        try {
            muc2.join("cuteboyx_max");
            muc2.invite(editText.getText().toString(), "Meet me in this excellent room");



        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }

        muc2.addInvitationRejectionListener(new InvitationRejectionListener() {
            public void invitationDeclined(String invitee, String reason) {
                // Do whatever you need here...
                Log.e("invitationDeclined", reason);
            }
        });

    }

    private void invitationListener() {

        managerreceiveInvitation = MultiUserChatManager.getInstanceFor(RoosterConnection.mConnection);
        managerreceiveInvitation.addInvitationListener(new InvitationListener() {
            @Override
            public void invitationReceived(XMPPConnection conn, String room, String inviter, String reason, String password, Message message) {
                try {
                    Log.e("declining invitation", room + " ok");

                    try {
                        MultiUserChat muc2 = managerreceiveInvitation.getMultiUserChat(room);
                        muc2.join("vishal" + System.currentTimeMillis());

                        muc2.addMessageListener(new MessageListener() {
                            @Override
                            public void processMessage(Message message) {
                                Log.e("msg received", message.getBody());
                            }
                        });

                        Intent intent = new Intent(GroupChatInviteActivity.this, GroupChatActivity.class);
                        startActivity(intent);
                    } catch (SmackException.NoResponseException e) {
                        e.printStackTrace();
                    } catch (XMPPException.XMPPErrorException e) {
                        e.printStackTrace();
                    }

                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
