package com.ggchat.rgi.ggchat.singletones;

/**
 * Created by rgi-40 on 30/3/17.
 */

public class GGChatHelper {
    private boolean chatActivityRunning;

    private static final GGChatHelper ourInstance = new GGChatHelper();

    public static GGChatHelper getInstance() {
        return ourInstance;
    }

    private GGChatHelper() {
    }

    public boolean isChatActivityRunning() {
        return chatActivityRunning;
    }

    public void setChatActivityRunning(boolean chatActivityRunning) {
        this.chatActivityRunning = chatActivityRunning;
    }
}
