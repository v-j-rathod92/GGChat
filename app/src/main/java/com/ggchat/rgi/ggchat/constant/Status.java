package com.ggchat.rgi.ggchat.constant;

/**
 * Created by rgi-40 on 29/3/17.
 */

public enum Status {
    SEND, SENT, READ, TYPING, TYPING_STOP;
}
