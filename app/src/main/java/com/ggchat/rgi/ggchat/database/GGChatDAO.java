package com.ggchat.rgi.ggchat.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ggchat.rgi.ggchat.model.Conversation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rgi-40 on 27/3/17.
 */

public class GGChatDAO {
    public static final String GET_CONVERSATION = "SELECT * FROM tbl_chat_history WHERE user_jid = ?";
    // public static final String GET_CONVERSATION_GROUP_BY = "select t1.* from tbl_chat_history t1 join (select user_jid, max(date) date from tbl_chat_history group by user_jid) t2 on t1.user_jid = t2.user_jid and t1.date = t2.date";
    public static final String GET_CONVERSATION_GROUP_BY = "SELECT id, user_jid, message, is_seen, type, max(date) date FROM tbl_chat_history GROUP BY user_jid ORDER BY date DESC";
    public static final String GET_TOTAL_UNREAD_MESSAGE = "SELECT COUNT(is_seen) unread FROM tbl_chat_history WHERE user_jid = ? and is_seen = ?";



    public static void insertChatHistory(String message, String withUserJid, String isSeen, String type, String date) {
        try {
            SQLiteDatabase database = DatabaseUtil.getDatabaseInstance();
            ContentValues cv = new ContentValues();

            cv.put("message", message);
            cv.put("user_jid", withUserJid);
            cv.put("is_seen", isSeen);
            cv.put("type", type);
            cv.put("date", date);

            database.insert("tbl_chat_history", null, cv);
            DatabaseUtil.closeResource(database, null, null);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    public static List<Conversation> getConversation(String withUserJid) {
        List<Conversation> listChatHistory = new ArrayList<>();
        Cursor cursor;

        try {
            SQLiteDatabase database = DatabaseUtil.getDatabaseInstance();

            cursor = database.rawQuery(GET_CONVERSATION, new String[] { withUserJid });

            while (cursor.moveToNext()) {
                Conversation conversation = new Conversation();

                conversation.setId(cursor.getString(cursor.getColumnIndex("id")));
                conversation.setWithUserJid(cursor.getString(cursor.getColumnIndex("user_jid")));
                conversation.setMessage(cursor.getString(cursor.getColumnIndex("message")));
                conversation.setIsSeen(cursor.getString(cursor.getColumnIndex("is_seen")));
                conversation.setType(cursor.getString(cursor.getColumnIndex("type")));
                conversation.setDate(cursor.getString(cursor.getColumnIndex("date")));

                listChatHistory.add(conversation);
            }

            DatabaseUtil.closeResource(database, null, cursor);
        } catch (DbException e) {
            e.printStackTrace();
        }

        return listChatHistory;
    }

    public static List<Conversation> getRecentChats() {

        Cursor cursor;
        List<Conversation> listChat = new ArrayList<>();

        try {
            SQLiteDatabase database = DatabaseUtil.getDatabaseInstance();
            cursor = database.rawQuery(GET_CONVERSATION_GROUP_BY, null);

            while (cursor.moveToNext()){
                Conversation conversation = new Conversation();

                conversation.setId(cursor.getString(cursor.getColumnIndex("id")));
                conversation.setWithUserJid(cursor.getString(cursor.getColumnIndex("user_jid")));
                conversation.setMessage(cursor.getString(cursor.getColumnIndex("message")));
                conversation.setIsSeen(cursor.getString(cursor.getColumnIndex("is_seen")));
                conversation.setType(cursor.getString(cursor.getColumnIndex("type")));
                conversation.setDate(cursor.getString(cursor.getColumnIndex("date")));

                listChat.add(conversation);
            }

            DatabaseUtil.closeResource(database, null, cursor);
        } catch (DbException e) {
            e.printStackTrace();
        }

        return listChat;
    }

    public static int getTotalUnreadMessage(String withUserJid){

        Cursor cursor;
        int totalUnread = 0;
        try {
            SQLiteDatabase database = DatabaseUtil.getDatabaseInstance();
            cursor = database.rawQuery(GET_TOTAL_UNREAD_MESSAGE, new String[] { withUserJid, "0"});
            cursor.moveToNext();

            totalUnread = cursor.getInt(cursor.getColumnIndex("unread"));

            DatabaseUtil.closeResource(database, null, cursor);
        } catch (DbException e) {
            e.printStackTrace();
        }

        return totalUnread;
    }

    public static void setAllReceivedMessageRead(String withUserId){

        try {
            SQLiteDatabase database = DatabaseUtil.getDatabaseInstance();

            ContentValues cv = new ContentValues();
            cv.put("is_seen", "1");

            database.update("tbl_chat_history", cv, "user_jid = ? AND is_seen = 0", new String[] { withUserId });

        } catch (DbException e) {
            e.printStackTrace();
        }


    }
}
