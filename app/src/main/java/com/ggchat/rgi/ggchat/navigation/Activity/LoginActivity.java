package com.ggchat.rgi.ggchat.navigation.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ggchat.rgi.ggchat.R;
import com.ggchat.rgi.ggchat.database.GGChatDAO;
import com.ggchat.rgi.ggchat.smack.RoosterConnection;
import com.ggchat.rgi.ggchat.smack.RoosterConnectionService;

/**
 * Created by rgi-40 on 25/3/17.
 */

public class LoginActivity extends AppCompatActivity {
    private EditText edtTxtEmail, edtTxtPwd;
    private Button btnLogin;
    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);

        init();
        setListeners();

    }

    private void init() {
        mContext = this;

        edtTxtEmail = (EditText) findViewById(R.id.edtTxtEmail);
        edtTxtPwd = (EditText) findViewById(R.id.edtTxtPwd);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("isLoggedIn", false)) {
//            startActivity(new Intent(LoginActivity.this, GropChatActivity.class));
            gotoHomeActivity();
            openConnection();
            finish();
        }
    }

    private void gotoHomeActivity() {
        startActivity(new Intent(mContext, MainActivity.class));
    }

    private void openConnection() {
        //Start the service
        Intent i1 = new Intent(this, RoosterConnectionService.class);
        startService(i1);
    }

    private void setListeners() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    gotoHomeActivity();
                    saveCredentials();
                    finish();
                }
            }
        });
    }

    private void saveCredentials() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        prefs.edit()
                .putString("xmpp_jid", edtTxtEmail.getText().toString())
                .putString("xmpp_password", edtTxtPwd.getText().toString())
                .putBoolean("isLoggedIn", true)
                .putBoolean("xmpp_logged_in", true)
                .commit();

        openConnection();
    }

    private boolean validate() {
        String email = edtTxtEmail.getText().toString();
        String pwd = edtTxtPwd.getText().toString();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(mContext,  getString(R.string.error_field_required), Toast.LENGTH_LONG).show();
            return false;
        } else if (!isEmailValid(email)) {
            Toast.makeText(mContext, getString(R.string.error_invalid_email), Toast.LENGTH_LONG).show();
            return false;
        } else if (TextUtils.isEmpty(pwd)) {
            Toast.makeText(mContext, getString(R.string.error_field_required), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }


    private boolean isEmailValid(String email) {
        return email.contains("@");
    }
}
