package com.ggchat.rgi.ggchat.listeners;

import com.ggchat.rgi.ggchat.model.Conversation;
import com.ggchat.rgi.ggchat.smack.Contact;

/**
 * Created by rgi-40 on 25/3/17.
 */

public interface OnRecyclerClickListener {
    public void onClick(int position);
}
