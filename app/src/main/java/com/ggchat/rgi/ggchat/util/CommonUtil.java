package com.ggchat.rgi.ggchat.util;

import android.app.ActivityManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.List;

/**
 * Created by rgi-40 on 7/4/17.
 */

public class CommonUtil {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;


    public static boolean checkIfAppIsOnForeground(Context mContext) {
        ActivityManager activityManager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = mContext.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {

            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }

        return false;
    }


    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = getConnectivityStatus(context);
        String status = null;
        if (conn == TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }


    /**
     * Check Network Aviablity
     *
     * @param context
     *            Context Object Of Activity
     * @return
     */
    public static boolean isOnline(Context context) {

		/*
		 * final ConnectivityManager connMgr = (ConnectivityManager)
		 * context.getSystemService(Context.CONNECTIVITY_SERVICE);
		 *
		 * final android.net.NetworkInfo wifi =
		 * connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		 *
		 * final android.net.NetworkInfo mobile =
		 * connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		 *
		 * if( wifi.isAvailable() ){ //Toast.makeText(context, "Wifi" ,
		 * Toast.LENGTH_LONG).show(); return true; } else if(
		 * mobile.isAvailable() ){ //Toast.makeText(contexst, "Mobile 3G " ,
		 * Toast.LENGTH_LONG).show(); return true; } else { //
		 * Toast.makeText(context, "No Network " , Toast.LENGTH_LONG).show();
		 * return false; }
		 */

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();

    }

}
