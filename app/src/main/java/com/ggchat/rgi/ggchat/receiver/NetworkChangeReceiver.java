package com.ggchat.rgi.ggchat.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.ggchat.rgi.ggchat.database.SharedPreferenceUtil;
import com.ggchat.rgi.ggchat.smack.RoosterConnection;
import com.ggchat.rgi.ggchat.smack.RoosterConnectionService;
import com.ggchat.rgi.ggchat.util.CommonUtil;

import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.IOException;

/**
 * Created by Vishal Rathod on 10/4/17.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("network changed", "ok");
        String status = CommonUtil.getConnectivityStatusString(context);

        if (CommonUtil.isOnline(context)) {
            if (SharedPreferenceUtil.getBoolean("isLoggedIn", false)) {
//                try {
//                    RoosterConnection mConnection = new RoosterConnection(context);
//                    mConnection.connect();
//                } catch (SmackException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (XMPPException e) {
//                    e.printStackTrace();
//                }
            }
        }
    }
}
