package com.ggchat.rgi.ggchat.smack;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class ChatData {

	private static final String TAG = "ChatData";
	public static final String DATABASE_NAME = "GGChatData";

	public static final String KEY_ROSTER_ID = "id";
	public static final String KEY_ROSTER_JID = "jid";
	public static final String KEY_ROSTER_NICKNAME = "nickname";
	public static final String KEY_ROSTER_STATUS = "status";
	public static final String KEY_ROSTER_ISFRIEND = "isfriend";
	public static final String KEY_ROSTER_TIME = "timestamp";

	public static final String TABLE_ROSTER = "tbl_roster";


	public  static  final String path= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath();



	private static final int DATABASE_VERSION = 1;

	private static final String CREATE_ROSTER = "create table tbl_roster(id integer primary key autoincrement, "
			+ "jid text not null, nickname text not null, status text not null, isfriend text not null, timestamp text not null);";
	
	private final Context context;

	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	public ChatData(Context context) {

		this.context = context;
		DBHelper = new DatabaseHelper(context);

	}

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			//super(context, DATABASE_NAME, null, DATABASE_VERSION);
			super(context,  DATABASE_NAME, null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			try {
				db.execSQL(CREATE_ROSTER);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
//			db.execSQL("DROP TABLE IF EXISTS " + TABLE_STUDENT);

			onCreate(db);
		}
	}

	// ---opens the database---
	public ChatData open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	// ---insert a Record into the database---
	public long insertRoster(String jid, String nickname,String isfriend) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_ROSTER_JID, jid);
		initialValues.put(KEY_ROSTER_NICKNAME, nickname);
		initialValues.put(KEY_ROSTER_ISFRIEND, isfriend);
		initialValues.put(KEY_ROSTER_STATUS, isfriend);
		initialValues.put(KEY_ROSTER_TIME, isfriend);

		return db.insert(TABLE_ROSTER, null, initialValues);
	}

	// --insert taluka



	public List<Contact> getAllRosterList(){
		List<Contact> mContacts = new ArrayList<>();

		Cursor mCursor = db.query(TABLE_ROSTER, new String[] {KEY_ROSTER_ID,KEY_ROSTER_JID, KEY_ROSTER_NICKNAME, KEY_ROSTER_STATUS, KEY_ROSTER_ISFRIEND,KEY_ROSTER_TIME},
				null, null, null, null, null);

		int i = 0;
		if (mCursor != null) {
			if (mCursor.getCount() > 0) {
				System.out.println(mCursor.getColumnCount());
				String strRec = "";
				mCursor.moveToFirst();
				do {
					mContacts.add(new Contact(mCursor.getString(mCursor.getColumnIndex(KEY_ROSTER_JID))));
					i++;
				} while (mCursor.moveToNext());
				mCursor.close();
			}
		}
		mCursor.close();

		return mContacts;
	}

	public boolean deleteAllRoster() {
		return db.delete(TABLE_ROSTER, null, null) > 0;
	}



	public boolean isRosterInserted(String jid){
		Cursor cursor = db.query(TABLE_ROSTER, new String[] { KEY_ROSTER_ID,KEY_ROSTER_JID, KEY_ROSTER_NICKNAME, KEY_ROSTER_STATUS, KEY_ROSTER_ISFRIEND,KEY_ROSTER_TIME},
				KEY_ROSTER_JID + " = '" + jid + "' ",
				null, null, null, null);
		Log.d("roster idd", jid);
		if (cursor.getCount() > 0) {
			return true;
		}else {
			return false;
		}
	}
	public Cursor getAllRoster() {
		return db.query(TABLE_ROSTER, new String[] {KEY_ROSTER_ID,KEY_ROSTER_JID, KEY_ROSTER_NICKNAME, KEY_ROSTER_STATUS, KEY_ROSTER_ISFRIEND,KEY_ROSTER_TIME},
				null, null, null, null,null);
	}
	public String getRosterStatus(String id) {

		Cursor cursor = db.query(TABLE_ROSTER, new String[] { KEY_ROSTER_ID,KEY_ROSTER_JID, KEY_ROSTER_NICKNAME, KEY_ROSTER_STATUS, KEY_ROSTER_ISFRIEND,KEY_ROSTER_TIME},
				KEY_ROSTER_JID + " = '" + id + "' ",
				null, null, null, null);
		Log.d("roster idd", id);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			if (cursor.moveToFirst()) {
				return cursor.getString(cursor.getColumnIndex(KEY_ROSTER_JID));
			} else {
				return "Error";
			}
		} else {
			return "Not Found";
		}
	}

}