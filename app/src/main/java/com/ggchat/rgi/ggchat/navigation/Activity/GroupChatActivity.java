package com.ggchat.rgi.ggchat.navigation.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ggchat.rgi.ggchat.R;
import com.ggchat.rgi.ggchat.smack.RoosterConnection;

import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;

/**
 * Created by Vishal Rathod on 13/4/17.
 */

public class GroupChatActivity extends AppCompatActivity {
    private ChatView chatView;
    private MultiUserChatManager managerSendInvitation;
    private MultiUserChat muc2;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_group_chat);

        init();
        setListener();
    }

    private void init(){
        chatView = (ChatView) findViewById(R.id.chatView);

        managerSendInvitation = MultiUserChatManager.getInstanceFor(RoosterConnection.mConnection);

        muc2 = managerSendInvitation.getMultiUserChat("ggchat@conference.globalgarner.com");

        try {
            muc2.join("testbot2" + System.currentTimeMillis());
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }

        muc2.addMessageListener(new MessageListener() {
            @Override
            public void processMessage(Message message) {
                Log.e("message received", message.getBody());
            }
        });
    }

    private void setListener(){
        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener() {
            @Override
            public boolean sendMessage(ChatMessage chatMessage) {

                try {
                    muc2.sendMessage(chatView.getTypedMessage());

                    Message msg = new Message("ggchat@conference.globalgarner.com",Message.Type.groupchat);
                    msg.setBody(chatView.getTypedMessage());
                    RoosterConnection.mConnection.sendPacket(msg);
                } catch (XMPPException e) {
                    e.printStackTrace();
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }

                return true;
            }
        });
    }
}
